FROM php:7.4-fpm-buster

ENV USER_ID=1000
ENV GROUP_ID=1000

RUN apt-get update \
    # s6 overlay
    && curl https://github.com/just-containers/s6-overlay/releases/download/v1.22.1.0/s6-overlay-amd64.tar.gz -o /tmp/s6-overlay.tar.gz -s -L \
    && tar xzf /tmp/s6-overlay.tar.gz -C / \
    # nginx
    && apt-get install -y --no-install-recommends nginx \
    && rm -rf /etc/nginx/sites-enabled/* \
    && ln -s /etc/nginx/sites-available/default.conf /etc/nginx/sites-enabled/default.conf \
    && mkdir -p /var/www/html/public \
    && mv /var/www/html/index.nginx-debian.html /var/www/html/public/index.html \
    # cron
    && apt-get install -y --no-install-recommends cron \
    # php
    && mv /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini \
    # gd
    && apt-get install -y --no-install-recommends libpng-dev libjpeg-dev libfreetype6-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install gd \
    # pdo_mysql
    && docker-php-ext-install pdo_mysql \
    # intl
    && apt-get install -y zlib1g-dev libicu-dev g++ \
    && docker-php-ext-install intl \
    # opcache
    && docker-php-ext-install opcache \
    # xdebug
    && pecl install xdebug \
    && docker-php-ext-enable xdebug \
    # essentials
    && apt-get install -y --no-install-recommends git unzip \
    # composer
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    # symfony
    && curl -sS https://get.symfony.com/cli/installer | bash \
    && mv /root/.symfony/bin/symfony /usr/local/bin/symfony \
    # utils
    && apt-get install -y --no-install-recommends mc procps

COPY ./configs /

EXPOSE 80

ENTRYPOINT ["/init"]
